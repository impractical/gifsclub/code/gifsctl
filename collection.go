package main

import (
	"context"
	"fmt"

	"gifs.club/code/go-gifs/gifs"
	"github.com/mitchellh/cli"
)

func collectionStartCommandFactory(client *gifs.Client, ui cli.Ui) func() (cli.Command, error) {
	return func() (cli.Command, error) {
		return collectionStartCommand{
			UI:     ui,
			client: client,
		}, nil
	}
}

type collectionStartCommand struct {
	UI     cli.Ui
	client *gifs.Client
}

func (c collectionStartCommand) Help() string {
	return "some very informative help text"
}

func (c collectionStartCommand) Run(args []string) int {
	coll := gifs.Collection{
		Slug: args[0],
		Name: args[1],
	}
	err := c.client.Collections().Create(context.Background(), coll)
	if err != nil {
		c.UI.Error("error creating collection: " + err.Error())
		return 1
	}
	c.UI.Info("Successfully created collection.")
	return 0
}

func (c collectionStartCommand) Synopsis() string {
	return "Create a new collection."
}

func collectionSetCommandFactory() (cli.Command, error) {
	return collectionSetCommand{}, nil
}

type collectionSetCommand struct{}

func (c collectionSetCommand) Help() string {
	return "some very informative help text"
}

func (c collectionSetCommand) Run(args []string) int {
	fmt.Printf("uploading %q, I guess?\n", args[0])
	return 0
}

func (c collectionSetCommand) Synopsis() string {
	return "Modify a collection's metadata."
}

func collectionRmCommandFactory() (cli.Command, error) {
	return collectionRmCommand{}, nil
}

type collectionRmCommand struct{}

func (c collectionRmCommand) Help() string {
	return "some very informative help text"
}

func (c collectionRmCommand) Run(args []string) int {
	fmt.Printf("uploading %q, I guess?\n", args[0])
	return 0
}

func (c collectionRmCommand) Synopsis() string {
	return "Remove a collection."
}

func collectionDescribeCommandFactory() (cli.Command, error) {
	return collectionDescribeCommand{}, nil
}

type collectionDescribeCommand struct{}

func (c collectionDescribeCommand) Help() string {
	return "some very informative help text"
}

func (c collectionDescribeCommand) Run(args []string) int {
	fmt.Printf("uploading %q, I guess?\n", args[0])
	return 0
}

func (c collectionDescribeCommand) Synopsis() string {
	return "Display information about a collection."
}

func collectionListCommandFactory() (cli.Command, error) {
	return collectionListCommand{}, nil
}

type collectionListCommand struct{}

func (c collectionListCommand) Help() string {
	return "some very informative help text"
}

func (c collectionListCommand) Run(args []string) int {
	fmt.Printf("uploading %q, I guess?\n", args[0])
	return 0
}

func (c collectionListCommand) Synopsis() string {
	return "List your collections."
}
